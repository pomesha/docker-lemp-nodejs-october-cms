Разработка прекращена.

# Настройка рабочего окружения для знакомства с October CMS. Docker Nginx NodeJS SQL PHP

## Инструменты:
1. Docker
2. SQL
3. Nginx
4. NodeJs
5. PHP
6. Git
7. October CMS

## Советы:
1. Настроить систему контроля версий. Справочник по адресу: https://git-scm.com/book/ru/v2
2. Папку с базой называть именем длинной больше 2-х символов, в папке из 2-х букв Git не видит файл .gitignore
3. Полезные команды Git: 

## October CMS
1. Заходим в контейнер, и выполняем команду "composer create-project october/october devoctober"
2. После установки composer - следуем инструкциям по установки October CMS https://octobercms.com/docs/setup/installation
a) composer create-project october/october myoctober
b) php artisan october:install
3. Выполняем команду в терминале контейнера: composer update
4. Ну и сама установка через команду в терминале - php artisan october:install.
	> Важно: т.к. установка в docker compose, то при настройке необходимо указать адрес MySQL Host > db
5. После окончания выдаст пароль для входа административную часть.
